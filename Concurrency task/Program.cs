﻿using System;

namespace Concurrency_task
{
    class Program
    {

        static void Main(string[] args)
        {
            var mainService = new MainService();
            mainService.Start();
            Console.ReadLine();
        }
    }
}
