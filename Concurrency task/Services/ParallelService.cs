﻿using Concurrency_task.Heplers;
using Concurrency_task.Interfaces;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Concurrency_task.Services
{
    public class ParallelService : IBase
    {
        public void Run()
        {
            Console.WriteLine("ParallelService started");
            Stopwatch timer = new Stopwatch();
            timer.Start();
            Find();
            timer.Stop();
            Console.WriteLine($"ParallelService ended {timer.Elapsed}");
        }

        private void Find()
        {
            var helper = new FindHelper();
            Parallel.Invoke(
                () => helper.RunFind(),
                () => helper.RunFind(),
                () => helper.RunFind());
        }
    }
}
