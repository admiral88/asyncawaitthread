﻿using Concurrency_task.Heplers;
using Concurrency_task.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Concurrency_task.Services
{
    public class ThreadPoolService : IBase
    {

        public void Run()
        {
            Console.WriteLine("ThreadPoolService started");
            Stopwatch timer = new Stopwatch();
            timer.Start();
            Action action = () => Find();
            var list = new List<Action>();
            list.Add(action);
            list.Add(action);
            list.Add(action);
            SpawnAndWait(list);
            timer.Stop();
            Console.WriteLine($"ThreadPoolService ended {timer.Elapsed}");
        }

        public static void SpawnAndWait(IEnumerable<Action> actions)
        {
            var list = actions.ToList();
            var handles = new ManualResetEvent[actions.Count()];
            for (var i = 0; i < list.Count; i++)
            {
                handles[i] = new ManualResetEvent(false);
                var currentAction = list[i];
                var currentHandle = handles[i];
                Action wrappedAction = () => { try { currentAction(); } finally { currentHandle.Set(); } };
                ThreadPool.QueueUserWorkItem(x => wrappedAction());
            }
            WaitHandle.WaitAll(handles);
        }

        public void Find()
        {
            var helper = new FindHelper();
            helper.RunFind();
        }
    }

}
