﻿using Concurrency_task.Heplers;
using Concurrency_task.Interfaces;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Concurrency_task.Services
{
    public class AsyncAwaitService : IBase
    {
        public void Run()
        {
            Console.WriteLine("AsyncAwaitService started");
            Stopwatch timer = new Stopwatch();
            timer.Start();
            Find();
            timer.Stop();
            Console.WriteLine($"AsyncAwaitService ended {timer.Elapsed}");
        }

        private void Find()
        {
            var helper = new FindHelper();
            //await Task.Run(() => helper.RunFind());

            var taskList = new Task[3]
            {
                new Task(()=> helper.RunFind()),
                new Task(()=> helper.RunFind()),
                new Task(()=> helper.RunFind())
            };
            foreach (var item in taskList)
            {
                item.Start();
            }
            Task.WaitAll(taskList);
        }
    }
}
