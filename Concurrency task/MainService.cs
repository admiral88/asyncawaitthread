﻿using Concurrency_task.Interfaces;
using Concurrency_task.Services;
using System.Collections.Generic;

namespace Concurrency_task
{
    public class MainService
    {
        public void Start()
        {
            var threadPoolService = new ThreadPoolService();
            var asyncAwaitService = new AsyncAwaitService();
            var parallelService = new ParallelService();

            var listOfServices = new List<IBase>
            {
                threadPoolService,
                asyncAwaitService,
                parallelService
            };

            foreach (var item in listOfServices)
            {
                item.Run();
            }
        }
    }
}
