﻿namespace Concurrency_task.Interfaces
{
    public interface IBase
    {
        void Run();
    }
}
